var gulp = require('gulp'),
  uglify = require('gulp-uglify'),
  pump = require('pump'),
  sass = require('gulp-ruby-sass'),
  htmlmin = require('gulp-htmlmin'),
  livereload = require('gulp-livereload'),
  plumber = require('gulp-plumber'),
  inject = require('gulp-inject'),
  concat = require('gulp-concat'),
  copy = require('gulp-copy');

gulp.task('watch' , function () {
  livereload.listen();
  gulp.watch('client/scripts/**/*.js',['concat']);
  gulp.watch('client/styles/**/*.scss', ['sass']);
  gulp.watch('client/**/*.html', ['html','inject']);
});

//compile sass
gulp.task('sass', function (cb) {
    sass('client/styles/sass/*.scss')
      .on('error', sass.logError)
      .pipe(plumber())
      .pipe(gulp.dest('public/styles'))
      .pipe(livereload());
    cb();
});

//compress all js file
gulp.task('min', function (cb) {
  pump([
      gulp.src('public/scripts/*.js'),
      uglify(),
      gulp.dest('public/scripts')
    ]
  ).pipe(plumber())
    .pipe(livereload());
  cb();
});

gulp.task('html', function(cb){
   gulp.src('client/index.html')
    .pipe(plumber())
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('public'))
    .pipe(livereload());
  cb();
});

gulp.task('inject', function(){
  setTimeout(function () {
    var sources = gulp.src(['public/scripts/*.js', 'public/styles/**/*.css'], {read: false});

    gulp.src('./public/index.html')
      .pipe(plumber())
      .pipe(inject(sources, {relative: true}))
      .pipe(gulp.dest('./public'))
      .pipe(livereload());
  },2000)
});

gulp.task('concat', function(cb){
  var sources = ['./client/scripts/**/*.js'];

  gulp.src(sources)
    .pipe(plumber())
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/scripts'))
    .pipe(livereload());
  cb();
});

gulp.task('copy:video', function(cb){
  var sources = ['./client/videos/*.*'];

  gulp.src(sources)
    .pipe(gulp.dest('./public/videos'));
  cb();
});

gulp.task('copy:templates', function(cb){
  var sources = ['./client/templates/*.*'];

  gulp.src(sources)
    .pipe(gulp.dest('./public/templates'));
  cb();
});



gulp.task('build', ['copy:video','copy:templates','html','concat','sass','min'], function(){
  gulp.start('inject');
});

gulp.task('default', ['build','watch']);
